const mysql = require('mysql');
const express = require('express');
const app = express();
const cors = require('cors');

/*
*   Indicando que la aplicación utilice cors
*/
app.use(cors());

/*
*   @IP, es la ip en la que se estara ejecutando la aplicación
*   @PORT, el puerto en el que escucha la aplicación.
*   utlilizando las varibles de entorno o las establecidas posteriormente.
*/
const PORT = process.env.PORT || 3002;
const IP = process.env.IP || "172.10.10.4";

/*
*   Se establecen las variables para la conexión con la base de datos
*   Seran utlizadas las variables de entorno o las que se encuentran posteriormente
*   @DB_HOST, es el host o IP de en donde se encuentra el servidor de base de datos
*   @DB_USERNAME, es el usuario mediante el cual se conectara a la base de datos
*   @DB_PASSWORD, es la contraseña por medio de la cual se conectara a la base de datos
*   @DB_NAME, es el nombre de la base de datos a la cual se conectara la app
*/
const DB_HOST = process.env.DB_HOST || "172.10.10.5";
const DB_USERNAME = process.env.DB_USERNAME || "admin";
const DB_PASSWORD = process.env.DB_PASSWORD || "admin1234";
const DB_NAME = process.env.DB_NAME || "sa_practica8";

/*
*   Conexión a la base de datos
*/
var connection = mysql.createConnection({
    host: DB_HOST,
    user: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME
});

/*
*   Este metodo se encarga de retoronar todos los usuarios que han sido almacenados
*   en la base de datos, mediante la conexión a la base de datos establecida anteriormente.
*/
app.get('/getUsers/', function (req, res) {
    var sql = "SELECT * FROM usuario;";

    connection.query(sql, function (err, records) {
        if (err) throw err;
        res.send(JSON.parse(JSON.stringify(records)));
    });
});

/*
*   Iniciar la aplicación para que es escuche en la IP y PUERTO indicados
*/
app.listen(PORT, IP, () => {
    console.log('La aplicación se encuentra escuchando en: http://' + IP + ':' + PORT);
});

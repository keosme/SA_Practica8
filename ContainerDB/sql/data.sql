CREATE TABLE usuario(
    id INT PRIMARY KEY AUTO_INCREMENT,
    carnet VARCHAR(12) UNIQUE,
    nombre VARCHAR(255),
    carrera VARCHAR(255)
);

INSERT INTO usuario(carnet, nombre, carrera) VALUES ("201612460","Kevin Oswaldo Mejía Lemus", "Ingeneiria en Ciencias y Sistemas");
INSERT INTO usuario(carnet, nombre, carrera) VALUES ("201612461","Kevin Oswaldo Mejía Lemus 2", "Ingeneiria en Ciencias y Sistemas");


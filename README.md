# Software Avanzado Practica 8

Esta practica consiste en la creación de 2 contenedores, un contenedor posee un servidor de base de datos que en este caso se escogio mysql y el segundo contenedor que contiene un servidor web que se comunica al primer contenedor y obtiene su información.

## Construcción de las imagenes utilizando Dockerfiles
Se deben constuir las imagenes de docker, para ello se deben ejecutar las siguientes instrucciones en la raiz de este proyecto:

```
$ cd ContainerDB
$ sudo docker build -t dbserver .
$ cd ..
$ cd ContainerWebApp
$ sudo docker build -t webapp .
```

## Crear los contenedores con el uso de docker-compose
Una vez ya se tienen las imagenes de docker creadas, se utilizara docker-compose para crear una red de contenedores para la comunicacion entre el contenedor del servidor web y el contenedor de la base de datos. Para crear esta red se debe ejecutar el siguiente comando:

```
$ sudo docker-compose up
```

## Link del video
- https://youtu.be/0FmutJsIa9g
